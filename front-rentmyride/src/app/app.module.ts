import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.route';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MainAdminComponent } from './components/main-admin/main-admin.component';
import { MainEmpleadosComponent } from './components/main-empleados/main-empleados.component';
import { MainRentasComponent } from './components/main-rentas/main-rentas.component';
import { MainClientesComponent } from './components/main-clientes/main-clientes.component';
import { MainVehiculosComponent } from './components/main-vehiculos/main-vehiculos.component';
import { MainCompactosComponent } from './components/main-compactos/main-compactos.component';
import { MainSedanComponent } from './components/main-sedan/main-sedan.component';
import { MainSUVComponent } from './components/main-suv/main-suv.component';
import { MainCamionetasComponent } from './components/main-camionetas/main-camionetas.component';
import { MainVanComponent } from './components/main-van/main-van.component';
import { MainLujoComponent } from './components/main-lujo/main-lujo.component';
import { MainDeportivosComponent } from './components/main-deportivos/main-deportivos.component';
import { MainClasicosComponent } from './components/main-clasicos/main-clasicos.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainAdminComponent,
    MainEmpleadosComponent,
    MainRentasComponent,
    MainClientesComponent,
    MainVehiculosComponent,
    MainCompactosComponent,
    MainSedanComponent,
    MainSUVComponent,
    MainCamionetasComponent,
    MainVanComponent,
    MainLujoComponent,
    MainDeportivosComponent,
    MainClasicosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModalModule,
    AppRoutingModule,
    RouterModule.forRoot(ROUTES, { useHash: true }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
