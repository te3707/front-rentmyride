import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-lujo',
  templateUrl: './main-lujo.component.html',
  styleUrls: ['./main-lujo.component.css']
})
export class MainLujoComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idLujo: any
  marcaLujo: any
  modeloLujo: any
  anioLujo: any
  statusLujo: any
  precioLujo: any
  placasLujo: any
  gasolinaLujo: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idLujoRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerLujo()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idLujoRenta = this.idLujo[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasLujo[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaLujo[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerLujo()
    if (this.statusLujo[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idLujo[i], [i])
    }
  }


  obtenerLujo() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idLujo = resp.respuesta.idsLujo
      this.marcaLujo = resp.respuesta.marcaLujo;
      this.modeloLujo = resp.respuesta.modeloLujo;
      this.anioLujo = resp.respuesta.anioLujo
      this.statusLujo = resp.respuesta.statusLujo;
      this.precioLujo = resp.respuesta.precioLujo;
      this.placasLujo = resp.respuesta.placasLujo
      this.gasolinaLujo = resp.respuesta.gasolinaLujo
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idLujoRenta


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idLujoRenta, this.datosVehiculo).subscribe((resp: any) => {

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
