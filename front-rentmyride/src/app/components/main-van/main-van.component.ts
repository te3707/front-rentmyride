import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-van',
  templateUrl: './main-van.component.html',
  styleUrls: ['./main-van.component.css']
})
export class MainVanComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idVan: any
  marcaVan: any
  modeloVan: any
  anioVan: any
  statusVan: any
  precioVan: any
  placasVan: any
  gasolinaVan: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idVanRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerVan()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idVanRenta = this.idVan[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasVan[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaVan[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerVan()
    if (this.statusVan[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idVan[i], [i])
    }
  }


  obtenerVan() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idVan = resp.respuesta.idsVan
      this.marcaVan = resp.respuesta.marcaVan;
      this.modeloVan = resp.respuesta.modeloVan;
      this.anioVan = resp.respuesta.anioVan
      this.statusVan = resp.respuesta.statusVan;
      this.precioVan = resp.respuesta.precioVan;
      this.placasVan = resp.respuesta.placasVan
      this.gasolinaVan = resp.respuesta.gasolinaVan
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idVanRenta


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idVanRenta, this.datosVehiculo).subscribe((resp: any) => {

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
