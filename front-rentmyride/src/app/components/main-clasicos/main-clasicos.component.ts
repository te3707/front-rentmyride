import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-clasicos',
  templateUrl: './main-clasicos.component.html',
  styleUrls: ['./main-clasicos.component.css']
})
export class MainClasicosComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idClasicos: any
  marcaClasicos: any
  modeloClasicos: any
  anioClasicos: any
  statusClasicos: any
  precioClasicos: any
  placasClasicos: any
  gasolinaClasicos: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idClasicosRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerClasicos()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idClasicosRenta = this.idClasicos[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasClasicos[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaClasicos[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerClasicos()
    if (this.statusClasicos[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idClasicos[i], [i])
    }
  }


  obtenerClasicos() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idClasicos = resp.respuesta.idsClasicos
      this.marcaClasicos = resp.respuesta.marcaClasicos;
      this.modeloClasicos = resp.respuesta.modeloClasicos;
      this.anioClasicos = resp.respuesta.anioClasicos
      this.statusClasicos = resp.respuesta.statusClasicos;
      this.precioClasicos = resp.respuesta.precioClasicos;
      this.placasClasicos = resp.respuesta.placasClasicos
      this.gasolinaClasicos = resp.respuesta.gasolinaClasicos
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idClasicosRenta


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idClasicosRenta, this.datosVehiculo).subscribe((resp: any) => {

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
