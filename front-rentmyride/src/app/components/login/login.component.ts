import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  datosLogin = { emailEmpleado: '', passwordEmpleado: '' };

  constructor(private _service: APIrestService, private _router: Router) { }

  ngOnInit(): void {
  }

  login() {
    if (this.datosLogin.emailEmpleado == '' || this.datosLogin.passwordEmpleado == '') {
      Swal.fire("¡Error!", 'Llene los campos', "error");
    } else {
      console.log(this.datosLogin);
      this._service.login(this.datosLogin).subscribe((resp: any) => {
        if (resp.role == 'Administrador') {
          Swal.fire("Éxito", "Bienvenido", "success");
          this._router.navigate(['mainAdmin']);
        }
        if (resp.mensaje == 'contraseña mal') {
          Swal.fire("¡Error!", 'Contraseña incorrecta', "error");
        }
        if (resp.mensaje == 'email no registrado') {
          Swal.fire("¡Error!", 'No existe el email', "error");
        }
      })
    }
  }

}
