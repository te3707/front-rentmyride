import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-suv',
  templateUrl: './main-suv.component.html',
  styleUrls: ['./main-suv.component.css']
})
export class MainSUVComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idSUV: any
  marcaSUV: any
  modeloSUV: any
  anioSUV: any
  statusSUV: any
  precioSUV: any
  placasSUV: any
  gasolinaSUV: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idSUVRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerSUV()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idSUVRenta = this.idSUV[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasSUV[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaSUV[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerSUV()
    if (this.statusSUV[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idSUV[i], [i])
    }
  }


  obtenerSUV() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idSUV = resp.respuesta.idsSUV
      this.marcaSUV = resp.respuesta.marcaSUV;
      this.modeloSUV = resp.respuesta.modeloSUV;
      this.anioSUV = resp.respuesta.anioSUV
      this.statusSUV = resp.respuesta.statusSUV;
      this.precioSUV = resp.respuesta.precioSUV;
      this.placasSUV = resp.respuesta.placasSUV
      this.gasolinaSUV = resp.respuesta.gasolinaSUV
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idSUVRenta
    console.log(this.datosRentaAdd);
    console.log(this.idSUVRenta);


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idSUVRenta, this.datosVehiculo).subscribe((resp: any) => {
        console.log(resp);

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
