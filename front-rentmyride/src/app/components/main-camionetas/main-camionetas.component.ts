import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-camionetas',
  templateUrl: './main-camionetas.component.html',
  styleUrls: ['./main-camionetas.component.css']
})
export class MainCamionetasComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idCamioneta: any
  marcaCamioneta: any
  modeloCamioneta: any
  anioCamioneta: any
  statusCamioneta: any
  precioCamioneta: any
  placasCamioneta: any
  gasolinaCamioneta: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idCamionetaRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerCamionetas()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idCamionetaRenta = this.idCamioneta[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasCamioneta[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaCamioneta[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerCamionetas()
    if (this.statusCamioneta[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idCamioneta[i], [i])
    }
  }


  obtenerCamionetas() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idCamioneta = resp.respuesta.idsCamionetas
      this.marcaCamioneta = resp.respuesta.marcaCamionetas;
      this.modeloCamioneta = resp.respuesta.modeloCamionetas;
      this.anioCamioneta = resp.respuesta.anioCamionetas
      this.statusCamioneta = resp.respuesta.statusCamionetas;
      this.precioCamioneta = resp.respuesta.precioCamionetas;
      this.placasCamioneta = resp.respuesta.placasCamionetas
      this.gasolinaCamioneta = resp.respuesta.gasolinaCamionetas
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idCamionetaRenta
    console.log(this.datosRentaAdd);
    console.log(this.idCamionetaRenta);


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idCamionetaRenta, this.datosVehiculo).subscribe((resp: any) => {
        console.log(resp);

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
