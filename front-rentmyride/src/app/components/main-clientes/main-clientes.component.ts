import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-clientes',
  templateUrl: './main-clientes.component.html',
  styleUrls: ['./main-clientes.component.css']
})
export class MainClientesComponent implements OnInit {


  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;
  @ViewChild("myModalInfo2", { static: false }) myModalInfo2!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf2", { static: false }) myModalConf2!: TemplateRef<any>;

  datosCliente: []

  datosClienteAdd = {
    nombresCliente: '', apellidosCliente: '', emailCliente: '', rfcCliente: '', statusCliente: '',
    telefonoCliente: '', rfcAvalCliente: ''
  };

  datosClientePut = {
    id: '', nombresCliente: '', apellidosCliente: '', emailCliente: '', rfcCliente: '', statusCliente: '',
    telefonoCliente: '', rfcAvalCliente: ''
  };

  mensaje: ''
  mensajeEmail: ''

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerCliente()
  }

  modalCliente() {
    this.modalService.open(this.myModalInfo);
  }

  modalActualizarCliente(id: any) {
    this.modalService.open(this.myModalInfo2);
    this.datosClientePut.id = id._id;
    this.datosClientePut.nombresCliente = id.nombresCliente;
    this.datosClientePut.apellidosCliente = id.apellidosCliente
    this.datosClientePut.emailCliente = id.emailCliente;
    this.datosClientePut.rfcCliente = id.rfcCliente
    this.datosClientePut.statusCliente = id.statusCliente
    this.datosClientePut.telefonoCliente = id.telefonoCliente
    this.datosClientePut.rfcAvalCliente = id.rfcAvalCliente

  }

  mostrarModalConf2(spot: any) {
    this.modalService.open(this.myModalConf2).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  obtenerCliente() {
    this._service.obtenerClientes()
      .subscribe((resp: any) => {
        this.datosCliente = resp.respuesta;
      })
  }

  agregarCliente() {
    this.datosClienteAdd.statusCliente = 'Activo'
    this._service.agregarCliente(this.datosClienteAdd).subscribe((resp: any) => {
      this.mensaje = resp.error;
      this.mensajeEmail = resp.mensaje
      if (resp.msg == 'Registro exitoso') {

        Swal.fire("Éxito", "Cliente agregado correctamente", "success")
        this.datosClienteAdd = {
          nombresCliente: '', apellidosCliente: '', emailCliente: '', rfcCliente: '', statusCliente: '',
          telefonoCliente: '', rfcAvalCliente: ''
        };
      }
      if (this.mensaje.length > 30) {
        const palabras = this.mensaje.split(" ")
        if (palabras[5] == 'least') {
          Swal.fire("¡Error!", palabras[0] + ' tiene que tener al menos 6 caracteres', "error");
        }
        if (palabras[6] == 'empty') {
          Swal.fire("¡Error!", palabras[0] + 'no puede estar vacio', "error");
        }
        if (palabras[5] == 'email') {
          Swal.fire("¡Error!", palabras[0] + ' debe ser un email válido', "error");
        }
        if (palabras[4] == 'number') {
          Swal.fire("¡Error!", palabras[0] + ' no puede estar vacio', "error");
        }
        if (palabras[0] == '"rfcCliente"') {
          Swal.fire("¡Error!", 'Ingrese un RFC válido', "error");
        }
      }
      if (resp.mensaje == "Email ya registrado") {
        Swal.fire("¡Error!", 'Email ya registrado', "error");
      }
      if (resp.mensaje == "RFC ya registrado") {
        Swal.fire("¡Error!", 'RFC ya registrado', "error");
      }
    })
    this.obtenerCliente()
  }

  actualizarCliente(id: string) {
    this._service.actualizarCliente(id, this.datosClientePut)
      .subscribe((resp: any) => {
        Swal.fire("Éxito", "Cliente actualizado correctamente", "success");
        this.obtenerCliente();
      })
  }

  eliminarCliente(id: any) {

    Swal.fire({
      title: 'Seguro que desea eliminar este Cliente?',
      text: "No podrás revertir este cambio!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        this._service.eliminarCliente(id).subscribe((resp: any) => {
        });
        Swal.fire(
          'Eliminado!',
          'Cliente eliminado exitosamente',
          'success'
        )
        this.obtenerCliente()
      }
    })
  }

}
