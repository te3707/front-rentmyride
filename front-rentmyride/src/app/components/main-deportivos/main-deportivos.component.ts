import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-deportivos',
  templateUrl: './main-deportivos.component.html',
  styleUrls: ['./main-deportivos.component.css']
})
export class MainDeportivosComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idDeportivos: any
  marcaDeportivos: any
  modeloDeportivos: any
  anioDeportivos: any
  statusDeportivos: any
  precioDeportivos: any
  placasDeportivos: any
  gasolinaDeportivos: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idDeportivosRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerDeportivos()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idDeportivosRenta = this.idDeportivos[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasDeportivos[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaDeportivos[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerDeportivos()
    if (this.statusDeportivos[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idDeportivos[i], [i])
    }
  }


  obtenerDeportivos() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idDeportivos = resp.respuesta.idsDeportivos
      this.marcaDeportivos = resp.respuesta.marcaDeportivos;
      this.modeloDeportivos = resp.respuesta.modeloDeportivos;
      this.anioDeportivos = resp.respuesta.anioDeportivos
      this.statusDeportivos = resp.respuesta.statusDeportivos;
      this.precioDeportivos = resp.respuesta.precioDeportivos;
      this.placasDeportivos = resp.respuesta.placasDeportivos
      this.gasolinaDeportivos = resp.respuesta.gasolinaDeportivos
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idDeportivosRenta


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idDeportivosRenta, this.datosVehiculo).subscribe((resp: any) => {

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
