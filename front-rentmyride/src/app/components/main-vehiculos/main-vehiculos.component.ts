import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { rejects } from 'assert';
// import { resolve } from 'dns';
// import { basename } from 'path';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { MainRentasComponent } from '../main-rentas/main-rentas.component';

@Component({
  selector: 'app-main-vehiculos',
  templateUrl: './main-vehiculos.component.html',
  styleUrls: ['./main-vehiculos.component.css']
})
export class MainVehiculosComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;
  @ViewChild("myModalInfo2", { static: false }) myModalInfo2!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf2", { static: false }) myModalConf2!: TemplateRef<any>;

  datosVehiculo: []

  datosVehiculoAdd = {
    noSerieVehiculo: '', marcaVehiculo: '', modeloVehiculo: '', tipoVehiculo: '', anioVehiculo: '',
    transmisionVehiculo: '', colorVehiculo: '', placasVehiculo: '', kilometrajeVehiculo: '', detallesVehiculo: '',
    polizaSeguroVehiculo: '', statusVehiculo: '', ultimoServicioVehiculo: '', gasolinaVehiculo: '', precioPorDiaVehiculo: '',
  };

  datosVehiculoPut = {
    id: '', noSerieVehiculo: '', marcaVehiculo: '', modeloVehiculo: '', tipoVehiculo: '', anioVehiculo: '',
    transmisionVehiculo: '', colorVehiculo: '', placasVehiculo: '', kilometrajeVehiculo: '', detallesVehiculo: '',
    polizaSeguroVehiculo: '', statusVehiculo: '', ultimoServicioVehiculo: '', gasolinaVehiculo: '', precioPorDiaVehiculo: '',
  };

  marcas: string[] = ['Volkswagen', 'Chevrolet', 'Toyota', 'Nissan', 'Ford', 'Honda', 'Hyundai', 'Mazda', 'Jeep', 'Mercedes', 'BMW']; // Agrega las marcas según tus necesidades
  modelosPorMarca: { [key: string]: string[] } = {
    Volkswagen: ['Polo', 'Tiguan', 'Jetta', 'Caddy', 'Golf', 'Passat', 'Beetle', 'Arteon', 'Touareg', 'Up'],
    Chevrolet: ['Astra', 'Camaro', 'Cruze', 'Equinox', 'Malibu', 'Silverado', 'Suburban', 'Tahoe', 'Traverse', 'Blazer', 'Impala'],
    Toyota: ['Camry', 'Corolla', 'RAV4', 'Highlander', 'Tacoma', 'Sienna', 'Prius', 'C-HR', '4Runner', 'Land Cruiser'],
    Nissan: ['Altima', 'Maxima', 'Sentra', 'Rogue', 'Pathfinder', 'Murano', 'Versa', 'Titan', 'Frontier', '370Z'],
    Ford: ['Mustang', 'Focus', 'Escape', 'Explorer', 'F-150', 'Edge', 'Fiesta', 'Ranger', 'Expedition', 'Bronco'],
    Honda: ['Civic', 'Accord', 'CR-V', 'Pilot', 'Odyssey', 'HR-V', 'Fit', 'Ridgeline', 'Insight', 'Passport'],
    Hyundai: ['Elantra', 'Sonata', 'Tucson', 'Santa Fe', 'Kona', 'Accent', 'Veloster', 'Palisade', 'Genesis', 'Venue'],
    Mazda: ['Mazda3', 'Mazda6', 'CX-5', 'CX-9', 'MX-5 Miata', 'CX-3', 'CX-30', 'MX-30', 'RX-8', 'BT-50'],
    Jeep: ['Wrangler', 'Cherokee', 'Grand Cherokee', 'Renegade', 'Compass', 'Gladiator', 'Wagoneer', 'Commander', 'Liberty', 'Patriot'],
    Mercedes: ['C-Class', 'E-Class', 'S-Class', 'GLC', 'GLE', 'A-Class', 'B-Class', 'CLA', 'GLA', 'GLS'],
    BMW: ['M3', 'M5', 'X6', 'i3', '8 Series', 'X4', 'M2', 'X2', 'X6 M', 'Z4']
  };

  vehiculo: any = {};

  public preview: string
  public fotos: any = []

  auxiliar: ''
  mensaje: ''
  mensajeEmail: ''

  constructor(private _service: APIrestService, private modalService: NgbModal, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.obtenerVehiculo()
  }

  cargarModelos() {
    const marcaSeleccionada = this.vehiculo.marca;
    if (marcaSeleccionada) {
      this.vehiculo.modelo = ''; // Reinicia el modelo seleccionado si cambia la marca
    }
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalActualizarVehiculo(id: any) {
    this.modalService.open(this.myModalInfo2);
    this.datosVehiculoPut.id = id._id;
    this.datosVehiculoPut.noSerieVehiculo = id.noSerieVehiculo;
    this.vehiculo.marca = id.marcaVehiculo
    this.vehiculo.modelo = id.modeloVehiculo;
    this.datosVehiculoPut.tipoVehiculo = id.tipoVehiculo
    this.datosVehiculoPut.anioVehiculo = id.anioVehiculo
    this.datosVehiculoPut.transmisionVehiculo = id.transmisionVehiculo
    this.datosVehiculoPut.colorVehiculo = id.colorVehiculo
    this.datosVehiculoPut.placasVehiculo = id.placasVehiculo
    this.datosVehiculoPut.kilometrajeVehiculo = id.kilometrajeVehiculo
    this.datosVehiculoPut.detallesVehiculo = id.detallesVehiculo
    this.datosVehiculoPut.polizaSeguroVehiculo = id.polizaSeguroVehiculo
    this.datosVehiculoPut.statusVehiculo = id.statusVehiculo
    this.auxiliar = id.ultimoServicioVehiculo
    var aux = this.auxiliar.split("T")
    this.datosVehiculoPut.ultimoServicioVehiculo = aux[0]
    this.datosVehiculoPut.gasolinaVehiculo = id.gasolinaVehiculo
    this.datosVehiculoPut.precioPorDiaVehiculo = id.precioPorDiaVehiculo

  }

  mostrarModalConf2(spot: any) {
    this.modalService.open(this.myModalConf2).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  // capturarFoto(event): any {
  //   const fotoSeleccionada = event.target.files[0]
  //   this.extraerBase64(fotoSeleccionada).then((imagen: any) => {
  //     this.preview = imagen.base
  //     console.log(imagen);
  //   })

  //   this.fotos.push(fotoSeleccionada)
  // }

  // extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
  //   try {
  //     const unsafeImg = window.URL.createObjectURL($event)
  //     const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg)
  //     const reader = new FileReader()
  //     reader.readAsDataURL($event)
  //     reader.onload = () => {
  //       resolve({
  //         base: reader.result
  //       })
  //     }
  //     reader.onerror = error => {
  //       resolve({
  //         base: null
  //       })
  //     }
  //   } catch (e) {
  //     return (e)
  //   }
  // });


  obtenerVehiculo() {
    this._service.obtenerVehiculos()
      .subscribe((resp: any) => {
        this.datosVehiculo = resp.respuesta;
      })
  }

  agregarVehiculo() {
    this.datosVehiculoAdd.statusVehiculo = 'Disponible'
    this.datosVehiculoAdd.marcaVehiculo = this.vehiculo.marca
    this.datosVehiculoAdd.modeloVehiculo = this.vehiculo.modelo
    this._service.agregarVehiculo(this.datosVehiculoAdd).subscribe((resp: any) => {

      this.mensaje = resp;
      this.mensajeEmail = resp.mensaje
      if (resp.msg == 'Registro exitoso') {

        Swal.fire("Éxito", "Vehiculo agregado correctamente", "success")
        location.reload()
        this.vehiculo.marca = ''
        this.vehiculo.modelo = ''
        this.datosVehiculoAdd = {
          noSerieVehiculo: '', marcaVehiculo: '', modeloVehiculo: '', tipoVehiculo: '', anioVehiculo: '',
          transmisionVehiculo: '', colorVehiculo: '', placasVehiculo: '', kilometrajeVehiculo: '', detallesVehiculo: '',
          polizaSeguroVehiculo: '', statusVehiculo: '', ultimoServicioVehiculo: '', gasolinaVehiculo: '', precioPorDiaVehiculo: '',
        };
        this.obtenerVehiculo()
      }
      if (this.mensaje.length > 20) {

        const palabras = this.mensaje.split(" ")
        if (palabras[0] == '"noSerieVehiculo"') {
          Swal.fire("¡Error!", 'Ingrese un número de serie', "error");
        }
        if (palabras[6] == '17') {
          Swal.fire("¡Error!", 'El número de serie debe tener 17 caracteres', "error");
        }
        if (palabras[9] == '17') {
          Swal.fire("¡Error!", 'El número de serie debe tener 17 caracteres', "error");
        }
        if (palabras[0] == '"marcaVehiculo"') {
          Swal.fire("¡Error!", 'Seleccione una marca', "error");
        }
        if (palabras[0] == '"modeloVehiculo"') {
          Swal.fire("¡Error!", 'Seleccione un modelo', "error");
        }
        if (palabras[0] == '"ultimoServicioVehiculo"') {
          Swal.fire("¡Error!", 'Seleccione una fecha', "error");
        }
      }
      if (resp.mensaje == "Vehiculo ya registrado") {
        Swal.fire("¡Error!", 'Vehiculo ya registrado', "error");
      }
      if (resp.mensaje == "Poliza de seguro ya registrada") {
        Swal.fire("¡Error!", 'Poliza ya registrada', "error");
      }
      if (resp.mensaje == "Placas ya registradas") {
        Swal.fire("¡Error!", 'Placas ya registradas', "error");
      }
    })
    this.obtenerVehiculo()
  }

  actualizarVehiculo(id: string) {
    this.datosVehiculoPut.marcaVehiculo = this.vehiculo.marca
    this.datosVehiculoPut.modeloVehiculo = this.vehiculo.modelo
    this._service.actualizarVehiculo(id, this.datosVehiculoPut)
      .subscribe((resp: any) => {
        Swal.fire("Éxito", "Vehiculo actualizado correctamente", "success");
        this.obtenerVehiculo();
      })
  }

  eliminarVehiculo(id: any) {

    Swal.fire({
      title: 'Seguro que desea eliminar este Vehiculo?',
      text: "No podrás revertir este cambio!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        this._service.eliminarVehiculo(id).subscribe((resp: any) => {
        });
        Swal.fire(
          'Eliminado!',
          'Vehiculo eliminado exitosamente',
          'success'
        )
        this.obtenerVehiculo()
      }
    })
  }

}
