import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-rentas',
  templateUrl: './main-rentas.component.html',
  styleUrls: ['./main-rentas.component.css']
})
export class MainRentasComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;
  @ViewChild("myModalInfo2", { static: false }) myModalInfo2!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf2", { static: false }) myModalConf2!: TemplateRef<any>;

  idActiva: any
  rfcClientesActivas: any
  fechasInicioActivas: any
  horaInicioACtivas: any
  fechasFinActivas: any
  placasActivas: any
  gasInicioActivas: any
  gasFinActivas: any
  statusActivas: any
  preciosActivas: any
  detallesActivas: any
  idsVehiculosActivas: any
  rfcClientesConcluidas: any
  fechasInicioConcluidas: any
  fechasFinConcluidas: any
  placasConcluidas: any
  gasInicioConcluidas: any
  gasFinConcluidas: any
  statusConcluidas: any
  preciosConcluidas: any
  detallesConcluidas: any

  datosRentaActiva: []
  datosRentaConcluida: []
  datosRentaFin = {
    idRenta: '', fechaHoraFinRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculo: ''
  }

  datosVehiculoPut = {
    idVehiculo: '', statusVehiculo: '', gasolinaVehiculo: ''
  }

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerRentasActivas()
  }


  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalTerminarRenta(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.datosRentaFin.idRenta = id
    this.datosRentaFin.idVehiculo = this.idsVehiculosActivas[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  obtenerRentasActivas() {
    this._service.obtenerRentasStatus()
      .subscribe((resp: any) => {
        this.idActiva = resp.respuesta.idsActivas
        this.rfcClientesActivas = resp.respuesta.rfcsRentasActivas
        this.fechasInicioActivas = resp.respuesta.fechaInicioActivas
        this.fechasFinActivas = resp.respuesta.fechaFinActivas
        this.placasActivas = resp.respuesta.placasActivas
        this.gasInicioActivas = resp.respuesta.gasolinaInicioActivas
        this.gasFinActivas = resp.respuesta.gasolinaFinActivas
        this.statusActivas = resp.respuesta.statusActivas
        this.preciosActivas = resp.respuesta.preciosActivas
        this.detallesActivas = resp.respuesta.detallesActivas
        this.idsVehiculosActivas = resp.respuesta.idsVehiculoActivas

        this.rfcClientesConcluidas = resp.respuesta.rfcsRentasconcluidas
        this.fechasInicioConcluidas = resp.respuesta.fechaInicioconcluidas
        this.fechasFinConcluidas = resp.respuesta.fechaFinconcluidas
        this.placasConcluidas = resp.respuesta.placasconcluidas
        this.gasInicioConcluidas = resp.respuesta.gasolinaInicioconcluidas
        this.gasFinConcluidas = resp.respuesta.gasolinaFinconcluidas
        this.statusConcluidas = resp.respuesta.statusconcluidas
        this.preciosConcluidas = resp.respuesta.preciosconcluidas
        this.detallesConcluidas = resp.respuesta.detallesconcluidas


      })

  }

  terminarRenta() {
    this.datosRentaFin.statusRenta = 'Concluida'
    this.datosVehiculoPut.idVehiculo = this.datosRentaFin.idVehiculo
    this.datosVehiculoPut.statusVehiculo = 'Disponible'
    this.datosVehiculoPut.gasolinaVehiculo = this.datosRentaFin.gasolinaFinRenta
    this._service.actualizarRenta(this.datosRentaFin.idRenta, this.datosRentaFin).subscribe((resp: any) => {
      this._service.actualizarFinRentaVehiculo(this.datosVehiculoPut.idVehiculo, this.datosVehiculoPut).subscribe((resp: any) => {

      })
      Swal.fire("Éxito", "Renta Registrada correctamente", "success");
      this.obtenerRentasActivas();
    })










    // Swal.fire({
    //   title: 'Seguro que desea eliminar este Cliente?',
    //   text: "No podrás revertir este cambio!",
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Borrar'
    // }).then((result) => {
    //   if (result.isConfirmed) {
    //     this._service.eliminarRenta(id).subscribe((resp: any) => {
    //     });
    //     Swal.fire(
    //       'Eliminado!',
    //       'Cliente eliminado exitosamente',
    //       'success'
    //     )
    //     this.obtenerRentasActivas()
    //   }
    // })
  }

}
