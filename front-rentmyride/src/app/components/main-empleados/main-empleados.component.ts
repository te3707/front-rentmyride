import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-empleados',
  templateUrl: './main-empleados.component.html',
  styleUrls: ['./main-empleados.component.css']
})
export class MainEmpleadosComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;
  @ViewChild("myModalInfo2", { static: false }) myModalInfo2!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf2", { static: false }) myModalConf2!: TemplateRef<any>;

  datosEmpleado: []

  datosEmpleadoAdd = {
    nombresEmpleado: '', apellidosEmpleado: '', emailEmpleado: '', roleEmpleado: '', sueldoEmpleado: '',
    rfcEmpleado: '', passwordEmpleado: '', statusEmpleado: '', telefonoEmpleado: ''
  };

  datosEmpleadoPut = {
    id: '', nombresEmpleado: '', apellidosEmpleado: '', emailEmpleado: '', roleEmpleado: '', sueldoEmpleado: '',
    rfcEmpleado: '', passwordEmpleado: '', statusEmpleado: '', telefonoEmpleado: ''
  };

  mensaje: ''
  mensajeEmail: ''

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerEmpleado()
  }

  modalEmpleado() {
    this.modalService.open(this.myModalInfo);
  }

  modalActualizarEmpleado(id: any) {
    this.modalService.open(this.myModalInfo2);
    this.datosEmpleadoPut.id = id._id;
    this.datosEmpleadoPut.nombresEmpleado = id.nombresEmpleado;
    this.datosEmpleadoPut.apellidosEmpleado = id.apellidosEmpleado
    this.datosEmpleadoPut.emailEmpleado = id.emailEmpleado;
    this.datosEmpleadoPut.roleEmpleado = id.roleEmpleado
    this.datosEmpleadoPut.sueldoEmpleado = id.sueldoEmpleado
    this.datosEmpleadoPut.rfcEmpleado = id.rfcEmpleado
    this.datosEmpleadoPut.passwordEmpleado = id.passwordEmpleado
    this.datosEmpleadoPut.statusEmpleado = id.statusEmpleado
    this.datosEmpleadoPut.telefonoEmpleado = id.telefonoEmpleado

  }

  mostrarModalConf2(spot: any) {
    this.modalService.open(this.myModalConf2).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  obtenerEmpleado() {
    this._service.obtenerEmpleados()
      .subscribe((resp: any) => {
        this.datosEmpleado = resp.respuesta;
      })
  }

  agregarEmpleado() {
    this.datosEmpleadoAdd.statusEmpleado = 'Activo'
    this._service.agregarEmpleado(this.datosEmpleadoAdd).subscribe((resp: any) => {
      this.mensaje = resp.error;
      this.mensajeEmail = resp.mensaje
      if (resp.msg == 'Registro exitoso') {

        Swal.fire("Éxito", "Empleado agregado correctamente", "success")
        location.reload()
        this.datosEmpleadoAdd = {
          nombresEmpleado: '', apellidosEmpleado: '', emailEmpleado: '', roleEmpleado: '', sueldoEmpleado: '',
          rfcEmpleado: '', passwordEmpleado: '', statusEmpleado: '', telefonoEmpleado: ''
        };
        this.obtenerEmpleado()
      }
      if (this.mensaje.length > 30) {
        const palabras = this.mensaje.split(" ")
        if (palabras[5] == 'least') {
          Swal.fire("¡Error!", palabras[0] + ' tiene que tener al menos 6 caracteres', "error");
        }
        if (palabras[6] == 'empty') {
          Swal.fire("¡Error!", palabras[0] + 'no puede estar vacio', "error");
        }
        if (palabras[5] == 'email') {
          Swal.fire("¡Error!", palabras[0] + ' debe ser un email válido', "error");
        }
        if (palabras[4] == 'number') {
          Swal.fire("¡Error!", palabras[0] + ' no puede estar vacio', "error");
        }
        if (palabras[0] == '"rfcEmpleado"') {
          Swal.fire("¡Error!", 'Ingrese un RFC válido', "error");
        }
      }
      if (resp.mensaje == "Email ya registrado") {
        Swal.fire("¡Error!", 'Email ya registrado', "error");
      }
      if (resp.mensaje == "RFC ya registrado") {
        Swal.fire("¡Error!", 'RFC ya registrado', "error");
      }
    })
    this.obtenerEmpleado()
  }

  actualizarEmpleado(id: string) {
    this._service.actualizarEmpleado(id, this.datosEmpleadoPut)
      .subscribe((resp: any) => {
        Swal.fire("Éxito", "Empleado actualizado correctamente", "success");
        this.obtenerEmpleado();
      })
  }

  eliminarEmpleado(id: any) {

    Swal.fire({
      title: 'Seguro que desea eliminar este empleado?',
      text: "No podrás revertir este cambio!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        this._service.eliminarEmpleado(id).subscribe((resp: any) => {
        });
        Swal.fire(
          'Eliminado!',
          'Empleado eliminado exitosamente',
          'success'
        )
        this.obtenerEmpleado()
      }
    })
  }

}
