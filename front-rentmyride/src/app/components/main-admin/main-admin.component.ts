import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-admin',
  templateUrl: './main-admin.component.html',
  styleUrls: ['./main-admin.component.css']
})
export class MainAdminComponent implements OnInit {

  cantidadCompacto: any
  cantidadSedan: any
  cantidadSUV: any
  cantidadCamionetas: any
  cantidadVan: any
  cantidadLujo: any
  cantidadDeportivos: any
  cantidadClasicos: any


  constructor(private _service: APIrestService) { }

  ngOnInit(): void {
    this.obetenerTipos()
  }

  obetenerTipos () {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.cantidadCompacto = resp.respuesta.cantidadCompactos;
      this.cantidadSedan = resp.respuesta.cantidadSedan;
      this.cantidadSUV = resp.respuesta.cantidadSUV;
      this.cantidadCamionetas = resp.respuesta.cantidadCamionetas;
      this.cantidadVan = resp.respuesta.cantidadVan;
      this.cantidadLujo = resp.respuesta.cantidadLujo;
      this.cantidadDeportivos = resp.respuesta.cantidadDeportivos;
      this.cantidadClasicos = resp.respuesta.cantidadClasicos;
    })
  }

}
