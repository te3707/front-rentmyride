import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-compactos',
  templateUrl: './main-compactos.component.html',
  styleUrls: ['./main-compactos.component.css']
})
export class MainCompactosComponent implements OnInit {


  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idCompacto: any
  marcaCompacto: any
  modeloCompacto: any
  anioCompacto: any
  statusCompacto: any
  precioCompacto: any
  placasCompacto: any
  gasolinaCompacto: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idCompactoRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerCompactos()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idCompactoRenta = this.idCompacto[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasCompacto[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaCompacto[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerCompactos()
    if (this.statusCompacto[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idCompacto[i], [i])
    }
  }


  obtenerCompactos() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idCompacto = resp.respuesta.idsCompactos
      this.marcaCompacto = resp.respuesta.marcaCompactos;
      this.modeloCompacto = resp.respuesta.modeloCompactos;
      this.anioCompacto = resp.respuesta.anioCompactos
      this.statusCompacto = resp.respuesta.statusCompactos;
      this.precioCompacto = resp.respuesta.precioCompactos;
      this.placasCompacto = resp.respuesta.placasCompactos
      this.gasolinaCompacto = resp.respuesta.gasolinaCompactos
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idCompactoRenta
    console.log(this.datosRentaAdd);
    console.log(this.idCompactoRenta);


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idCompactoRenta, this.datosVehiculo).subscribe((resp: any) => {
        console.log(resp);

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }


}
