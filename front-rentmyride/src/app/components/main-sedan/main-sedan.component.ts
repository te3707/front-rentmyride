import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APIrestService } from 'src/app/services/services/apirest.service';
import Swal from 'sweetalert2';
import { AppModule } from '../../app.module';

@Component({
  selector: 'app-main-sedan',
  templateUrl: './main-sedan.component.html',
  styleUrls: ['./main-sedan.component.css']
})
export class MainSedanComponent implements OnInit {

  @ViewChild("myModalInfo", { static: false }) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  @ViewChild("myModalConf", { static: false }) myModalConf!: TemplateRef<any>;

  idSedan: any
  marcaSedan: any
  modeloSedan: any
  anioSedan: any
  statusSedan: any
  precioSedan: any
  placasSedan: any
  gasolinaSedan: any

  datosRentaAdd = {
    rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
    gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
  }

  datosVehiculo = {
    statusVehiculo: 'En renta'
  }
  idSedanRenta: any


  rfcClientes: any

  constructor(private _service: APIrestService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.obtenerSedans()
    this.obtenerRFC()
  }

  modalVehiculo() {
    this.modalService.open(this.myModalInfo);
  }

  modalRentarVehiculo(id: any, pos: any) {
    this.modalService.open(this.myModalInfo);
    this.idSedanRenta = this.idSedan[pos]
    this.datosRentaAdd.placasVehiculoRenta = this.placasSedan[pos]
    this.datosRentaAdd.gasolinaInicioRenta = this.gasolinaSedan[pos]
  }

  mostrarModalConf(spot: any) {
    this.modalService.open(this.myModalConf).result.then(r => {
    }, error => {
      console.log(error);

    });
  }

  disponible(i: any) {
    this.obtenerSedans()
    if (this.statusSedan[i] != 'Disponible') {
      Swal.fire("¡Error!", 'Vehíchulo no disponible', "error");
    } else {
      this.modalRentarVehiculo(this.idSedan[i], [i])
    }
  }


  obtenerSedans() {
    this._service.obtenerVehiculosTipos().subscribe((resp: any) => {
      this.idSedan = resp.respuesta.idsSedan
      this.marcaSedan = resp.respuesta.marcaSedans;
      this.modeloSedan = resp.respuesta.modeloSedans;
      this.anioSedan = resp.respuesta.anioSedans
      this.statusSedan = resp.respuesta.statusSedans;
      this.precioSedan = resp.respuesta.precioSedans;
      this.placasSedan = resp.respuesta.placasSedans
      this.gasolinaSedan = resp.respuesta.gasolinaSedans
    })
  }

  obtenerRFC() {
    this._service.obtenerClientes().subscribe((resp: any) => {
      this.rfcClientes = resp.respuesta

    })
  }

  registrarRenta() {
    this.datosRentaAdd.statusRenta = 'Activa'
    this.datosRentaAdd.idVehiculoRenta = this.idSedanRenta


    this._service.agregarRenta(this.datosRentaAdd).subscribe((resp: any) => {

      this._service.actualizarStatusVehiculo(this.idSedanRenta, this.datosVehiculo).subscribe((resp: any) => {

      })
      Swal.fire("Éxito", "Renta Realizada correctamente", "success");
      this.datosRentaAdd = {
        rfcCliente: '', fechaHoraInicioRenta: '', fechaHoraFinRenta: '', placasVehiculoRenta: '',
        gasolinaInicioRenta: '', gasolinaFinRenta: '', statusRenta: '', precioRenta: '', detallesRenta: '', idVehiculoRenta: ''
      }

      location.reload()

    })
    // location.reload()
  }

}
