import { Injectable } from '@angular/core';
import { URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class APIrestService {

  public url;

  constructor(private _http: HttpClient) { this.url = URL; }

  login(body : any) {
    const url = `${this.url}/login`;
    return this._http.post(url, body);
  }

  obtenerEmpleados() {
    const url = `${this.url}/obtener/empleados/`;
    return this._http.get(url);
  }

  agregarEmpleado(body: any) {
    const url = `${this.url}/nuevo/empleado`;
    return this._http.post(url, body);
  }

  actualizarEmpleado(id : any, body : any) {
    const url = `${this.url}/update/empleado/${id}`;
    return this._http.put(url, body);
  }

  eliminarEmpleado(id: any) {
    const url = `${this.url}/delete/empleado/${id}`;
    return this._http.delete(url);
  }

  obtenerClientes() {
    const url = `${this.url}/obtener/clientes/`;
    return this._http.get(url);
  }

  agregarCliente(body: any) {
    const url = `${this.url}/nuevo/cliente`;
    return this._http.post(url, body);
  }

  actualizarCliente(id : any, body : any) {
    const url = `${this.url}/update/cliente/${id}`;
    return this._http.put(url, body);
  }

  eliminarCliente(id: any) {
    const url = `${this.url}/delete/cliente/${id}`;
    return this._http.delete(url);
  }

  obtenerVehiculos() {
    const url = `${this.url}/obtener/vehiculos/`;
    return this._http.get(url);
  }

  agregarVehiculo(body: any) {
    const url = `${this.url}/nuevo/vehiculo`;
    return this._http.post(url, body);
  }

  actualizarVehiculo(id : any, body : any) {
    const url = `${this.url}/update/vehiculo/${id}`;
    return this._http.put(url, body);
  }

  actualizarStatusVehiculo(id : any, body : any) {
    const url = `${this.url}/update/statusvehiculo/${id}`;
    return this._http.put(url, body);
  }

  actualizarFinRentaVehiculo(id : any, body : any) {
    const url = `${this.url}/update/finrentavehiculo/${id}`;
    return this._http.put(url, body);
  }

  eliminarVehiculo(id: any) {
    const url = `${this.url}/delete/vehiculo/${id}`;
    return this._http.delete(url);
  }

  obtenerVehiculosTipos () {
    const url = `${this.url}/obtener/vehiculos/tipos`;
    return this._http.get(url);
  }

  obtenerRentas() {
    const url = `${this.url}/obtener/rentas/`;
    return this._http.get(url);
  }

  obtenerRentasStatus () {
    const url = `${this.url}/obtener/rentas/status`;
    return this._http.get(url);
  }

  agregarRenta(body: any) {
    const url = `${this.url}/nuevo/renta`;
    return this._http.post(url, body);
  }

  eliminarRenta(id: any) {
    const url = `${this.url}/delete/renta/${id}`;
    return this._http.delete(url);
  }

  actualizarRenta(id : any, body : any) {
    const url = `${this.url}/update/renta/${id}`;
    return this._http.put(url, body);
  }
}

