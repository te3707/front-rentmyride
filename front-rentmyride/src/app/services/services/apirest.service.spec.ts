import { TestBed } from '@angular/core/testing';

import { APIrestService } from './apirest.service';

describe('APIrestService', () => {
  let service: APIrestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIrestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
