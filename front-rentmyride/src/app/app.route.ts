import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { LoginComponent } from './components/login/login.component';
import { MainAdminComponent } from './components/main-admin/main-admin.component';
import { MainClientesComponent } from './components/main-clientes/main-clientes.component';
import { MainEmpleadosComponent } from './components/main-empleados/main-empleados.component';
import { MainRentasComponent } from './components/main-rentas/main-rentas.component';
import { MainVehiculosComponent } from './components/main-vehiculos/main-vehiculos.component';
import { MainCompactosComponent } from './components/main-compactos/main-compactos.component';
import { MainClasicosComponent } from './components/main-clasicos/main-clasicos.component';
import { MainDeportivosComponent } from './components/main-deportivos/main-deportivos.component';
import { MainLujoComponent } from './components/main-lujo/main-lujo.component';
import { MainVanComponent } from './components/main-van/main-van.component';
import { MainCamionetasComponent } from './components/main-camionetas/main-camionetas.component';
import { MainSedanComponent } from './components/main-sedan/main-sedan.component';
import { MainSUVComponent } from './components/main-suv/main-suv.component';

export const ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'mainAdmin', component: MainAdminComponent },
    { path: 'mainClientes', component: MainClientesComponent },
    { path: 'mainEmpleados', component: MainEmpleadosComponent },
    { path: 'mainRentas', component: MainRentasComponent },
    { path: 'mainVehiculos', component: MainVehiculosComponent },
    { path: 'mainCompactos', component: MainCompactosComponent },
    { path: 'mainSedan', component: MainSedanComponent },
    { path: 'mainSUV', component: MainSUVComponent },
    { path: 'mainCamionetas', component: MainCamionetasComponent },
    { path: 'mainVan', component: MainVanComponent },
    { path: 'mainLujo', component: MainLujoComponent },
    { path: 'mainDeportivos', component: MainDeportivosComponent },
    { path: 'mainClasicos', component: MainClasicosComponent },

    { path: '', pathMatch: 'full', redirectTo: 'login' },
];

